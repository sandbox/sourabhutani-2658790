*********************************************************
-----------------------Video Schema-------------------
*********************************************************

Introductions
------------
Video Schema is used create schema for videos.
It is basically useful for SEO of videos type data.

*On enable module first creates a field type name Video schema.
*It will add 7 fields in which
(Video Name,Video Description,Video Thumbnail url,Upload date,
 Video Duration)
are mandatory
and (Video Content url,Video Embed url) are optional fields.


 Installation & Use
-------------------
* Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


Limitation
------------

* Creates schema only for video.
* Creates schema only in json ld format not in any other format.
